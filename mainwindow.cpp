#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QSqlDatabase>
#include <QDebug>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_tabWidget_destroyed()
{
    QSqlDatabase db =  QSqlDatabase::database();
    if (db.isOpen()) {
        qDebug() << "Closing connection" << endl;
        db.close();
    }
    else {
        qDebug() << "No need to close the DB cnx" << endl;
    }

}
