#include "mainwindow.h"
#include <QApplication>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QDir>
#include <QSqlDatabase>
#include <QSqlError>

#include "domuser.h"

void setTheApplicationStyle(QApplication& a) {
    QFile file("../../../style.css");
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
         QTextStream stream(&file);
         QString fileContent = stream.readAll();
         //qDebug() << "Contenu de la feuille de Style" << fileContent << endl;
         a.setStyleSheet(fileContent);
         file.close();
    }
}



int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    setTheApplicationStyle(a);

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("../../../mydb.sqlite1");
    if (!db.open()) {
        qDebug("Error occurred opening the database.");
        qDebug("%s.", qPrintable(db.lastError().text()));
        return -1;
    }
    else {
        qDebug() << "Connection DB OK";
    }
    DomUser du(1, "Frank", "New York");
    //qDebug() << du.getCount();
    du.add();
    MainWindow w;
    w.show();

    return a.exec();
}
