#ifndef DOMBASE_H
#define DOMBASE_H
#include <QString>
#include <QVariant>
#include <QSqlQuery>
#include <vector>
using namespace std;

/**
 * @brief The DomBase class
 * Le but de cette classe est de centraliser les
 * requêtes SQL afin d'éviter la redondance et le
 * copy paste dans le code
 */


class DomBase
{

public:

    virtual string getTableName() const = 0;
    virtual vector<string> getColumnList() const = 0;

    int getCount() const;

protected:
    bool _add(map<string, QVariant>& values) const;
    void bindInsertQuery(QSqlQuery &query,
                             map<string, QVariant>& values) const;

};

#endif // DOMBASE_H
