#-------------------------------------------------
#
# Project created by QtCreator 2015-09-05T23:01:55
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = last
TEMPLATE = app

DESTDIR = $$PWD

SOURCES += main.cpp\
        mainwindow.cpp \
    dombase.cpp \
    domuser.cpp

HEADERS  += mainwindow.h \
    dombase.h \
    domuser.h

FORMS    += mainwindow.ui

OTHER_FILES += \
    style.css
