#ifndef DOMUSER_H
#define DOMUSER_H
#include "dombase.h"

class DomUser : public DomBase
{
    int id;
    QString name;
    QString adress;

public:
    DomUser(int id, QString name, QString adress);
    string getTableName() const;
    vector<string> getColumnList() const;

    bool add() const;
};

#endif // DOMUSER_H
