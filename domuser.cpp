#include "domuser.h"

DomUser::DomUser(int id, QString name, QString adress)
    : id(id), name(name), adress(adress)
{
}

string DomUser::getTableName() const {
    return "User";
}

vector<string> DomUser::getColumnList() const {
    vector<string> l;
    l.push_back("id1");
    l.push_back("NAME");
    l.push_back("ADRESS");
    return l;
}

bool DomUser::add() const {
    map<string, QVariant> m;
    m["id1"] =  QVariant(QVariant::Int); // To bind a NULL value: http://stackoverflow.com/questions/338809/how-to-insert-a-null-value-with-qt
    m["NAME"] =  name;
    m["ADRESS"] = adress;

    return _add(m);
}
