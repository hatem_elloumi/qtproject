#include "dombase.h"
#include <QSqlQuery>
#include <QSqlDatabase>
#include <QSqlError>
#include <QVariant>
#include <iosfwd>
#include <sstream>
#include <QDebug>

/**
 * This method is downloaded from the Web and enables the concatenation of strings
 * joined by a separator
 */
template <typename Iter>
string join(Iter begin, Iter end, std::string const& separator)
{
  ostringstream result;
  if (begin != end)
    result << *begin++;
  while (begin != end)
    result << separator << *begin++;
  return result.str();
}

bool executeQuery(QSqlDatabase& db, QSqlQuery& queryObj, QString& queryTxt) {
    queryObj.prepare(queryTxt);
    if (!queryObj.exec()) {
        qDebug("Error occurred while executing SQL.");
        qDebug("%s.", qPrintable(db.lastError().text()));
        return false;
    }
    return true;
}

int DomBase::getCount() const {
    ostringstream os;
    os << "SELECT COUNT(*) FROM " << getTableName();
    QString select =  QString::fromStdString(os.str());
    QSqlDatabase db =  QSqlDatabase::database();
    QSqlQuery query(db);

    if (!executeQuery(db, query, select))
        return -1;

    query.next();
    qDebug("id = %d", query.value(0).toInt());
    return query.value(0).toInt();
}


void DomBase::bindInsertQuery(QSqlQuery &query,
                              map<string, QVariant>& values) const {
    //
    vector<string> cols = getColumnList();
    for (vector<string>::size_type i = 0; i < cols.size(); ++i) {
        QString key = ":"+ QString::fromStdString(cols[i]);
        qDebug() << "query.bindValue(" << key << ", " << values[cols[i]]<< ")";
        query.bindValue(key, values[cols[i]]);
    }

}

bool DomBase::_add(map<string, QVariant>& values) const {

    QSqlDatabase db =  QSqlDatabase::database();
    QSqlQuery query(db);
    ostringstream os;
    os << "INSERT INTO " << getTableName() <<  " (";
    vector<string> cols = getColumnList();
    os << join(cols.begin(), cols.end(), ", ");
    os << ") VALUES (:";
    os << join(cols.begin(), cols.end(), ", :");
    os << ") ";

    if (!query.prepare(QString::fromStdString(os.str()))) {
        qDebug("Error occurred while preparing SQL.");

    }

    bindInsertQuery(query, values);

    if (!query.exec()) {
        qDebug("Error occurred while executing SQL.");
        qDebug("%s.", qPrintable(db.lastError().text()));
        return false;
    }

    db.commit();
    return true;
}


